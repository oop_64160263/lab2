import java.util.Scanner;

public class YourGrade {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Please input your score:");
        String Strscore = sc.next();
        int score = Integer.parseInt(Strscore);
        String grade;
        if (score>=80){
            grade = "A";
        }else if (score<80 && score>=75){
            grade ="B+";
        }else if (score<75 && score>=70){
            grade ="B";
        }else if (score<70 && score>=65){
            grade ="C+";
        }else if (score<65 && score>=60){
            grade ="C";
        }else if (score<60 && score>=55){
            grade = "D+";
        }else if (score<55 && score>=50){
            grade = "D";
        }else{
            grade = "F";
        }     
        System.out.println("Grade: " + grade);
}
}